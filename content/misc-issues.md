---
title: "Minor Issues"
author: ["Jacob Hilker"]
draft: false
enableToc: true
---

## Dark Mode {#dark-mode}

So many websites are practically blinding these days it makes the internet hard to use. Whether you are working at night or just prefer a darker theme, using the internet without a dark mode is just painful.


### The Solution {#the-solution}

Until more sites implement a dark mode, I would try using something like Dark Reader, a browser extension that adds dark mode to websites. You could also talk to the admins of the website to try and add a dark mode.
