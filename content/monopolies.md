---
title: "Monopolies"
author: ["Jacob Hilker"]
draft: false
enableToc: true
---

I imagine that many of us are familiar with many of the major companies based on the internet today - in the tech industry the biggest ones have their own abbreviation: FAANG, for Facebook, Apple, Amazon, Netflix, and Google. Despite the fact that so many people use these sites, these users are still constantly tracked. Not to mention that these companies are used so often that there is practically no real competition betwen them in their respective fields.


## The Solution {#the-solution}

The solution is a simple one, although there isn't much a general user can do. We have to demand that these companies be broken up into much smaller companies. It's not ideal that the regular person can't really do anything about it, but I'm not sure what else we caan do, besides deleting account on those sites.
